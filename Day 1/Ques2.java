/***
 * 
 * @author Neha
 *
 */
package com.day1;


class Calculation {
	int firstNo = 20;
	int secondNo = 4;

	void add() {
		int sum = firstNo + secondNo;
		System.out.println("\n\nAddition :" + sum);
	}

	void substract() {
		int sub = firstNo - secondNo;
		System.out.println("Substraction :" + sub);
	}

	void multiply() {
		int mul = firstNo * secondNo;
		System.out.println("Multiplication :" + mul);
	}

	void divide() {
		int div = firstNo / secondNo;
		System.out.println("Division :" + div);
	}
}

public class Ques2 {

	public static void main(String[] args) {
		Calculation calculation = new Calculation();
		calculation.add();
		calculation.substract();
		calculation.multiply();
		calculation.divide();

	}

}
