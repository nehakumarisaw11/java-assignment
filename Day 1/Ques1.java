/***
 * 
 * @author Neha kumari saw(51932986)
 *
 */
package com.day1;

class Operation1{
	void operation() {
		int a = -5 + 8 * 6;
		int b = (55 + 9) % 9;
		int c = 20 + (-3 * 5 / 8);
		int d = 5 + 15 / 3 * 2 - 8 % 3;
		System.out.println("\n\nResult of operations:");
		System.out.print("a=" + a + "\nb=" + b + "\nc=" + c + "\nd=" + d);
		
	}
}



public class Ques1 {

	public static void main(String[] args) {
		Operation1 operation1 = new Operation1();
		operation1.operation();
		
		
		

	}

}
