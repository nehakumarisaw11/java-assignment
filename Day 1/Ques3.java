/***
 * 
 * @author Neha
 *
 */
package com.day1;


class Average {
	void average(int firstNo, int secondNo, int thirdNo) {
		int sum = 0;
		int avg = 0;
		sum = firstNo + secondNo + thirdNo;
		avg = sum / 3;
		System.out.println("\n\nAverage of Three Numbers (" + firstNo + "," + secondNo + "," + thirdNo + ")=" + avg);
	}

}
public class Ques3 {
	public static void main(String args[]) {
	Average average1 = new Average();
	average1.average(5,6,7);
	}


}
